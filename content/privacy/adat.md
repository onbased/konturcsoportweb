**A KONTÚR CSOPORT KFT.**

**ADATVÉDELMI TÁJÉKOZTATÓJA**

(hatályos: 2019. 09. 16-tól)

# **Tartalom**

[**1. ADATVÉDELMI TÁJÉKOZTATÓ CÉLJA, ALAPELVEI**
3](#adatvédelmi-tájékoztató-célja-alapelvei)

[**2. ALAPFOGALMAK** 3](#alapfogalmak)

[**Adatkezelés** 3](#_Toc34035414)

[**Adatkezelő** 3](#_Toc34035415)

[**Adatfeldolgozás** 3](#adatfeldolgozás)

[**Adatfeldolgozó** 4](#adatfeldolgozó)

[**Adatvédelmi incidens** 4](#adatvédelmi-incidens)

[**Címzett** 4](#címzett)

[**Érintett** 4](#érintett)

[**Felügyeleti hatóság (NAIH)** 4](#felügyeleti-hatóság-naih)

[**Hozzájárulás** 4](#hozzájárulás)

[**Személyes adat** 4](#személyes-adat)

[**3. ADATKEZELŐ** 4](#adatkezelő-1)

[**4. KEZELT ADATOK, ADATKEZELÉS JOGALAPJA, CÉLJA, HATÁRIDEJE**
4](#kezelt-adatok-adatkezelés-jogalapja-célja-határideje)

[**4.1. Önéletrajzok, bemutatkozó anyagok kezelése**
4](#önéletrajzok-bemutatkozó-anyagok-kezelése)

[**4.2. Információkérés, ajánlatkérés során végzett adatkezelés**
5](#információkérés-ajánlatkérés-során-végzett-adatkezelés)

[**4.3. Társaság által üzemeltetett weboldallal összefüggő adatkezelés**
5](#társaság-által-üzemeltetett-weboldallal-összefüggő-adatkezelés)

[**4.4. Szerződésekben feltüntetett személyes adatok kezelése**
5](#szerződésekben-feltüntetett-személyes-adatok-kezelése)

[**5. ADATKEZELÉS MÓDJA, ADATBIZTONSÁG**
6](#adatkezelés-módja-adatbiztonság)

[**6. AZ ÖN JOGAI, JOGORVOSLATI LEHETŐSÉGEI**
6](#az-ön-jogai-jogorvoslati-lehetőségei)

[**6.1. Az Ön jogai az adatkezelésünk során**
6](#az-ön-jogai-az-adatkezelésünk-során)

[**6.2. Amennyiben Társaságunktól tájékoztatást kér**
7](#amennyiben-társaságunktól-tájékoztatást-kér)

[**6.3. Amennyiben panaszával hozzánk fordul**
7](#amennyiben-panaszával-hozzánk-fordul)

[**6.4. Amennyiben a felügyelő hatósághoz kíván fordulni**
8](#amennyiben-a-felügyelő-hatósághoz-kíván-fordulni)

[**6.5. Amennyiben a bírósági eljárást választja**
8](#amennyiben-a-bírósági-eljárást-választja)

[**7. EGYÉB ADATKEZELÉSI KÉRDÉSEK** 8](#egyéb-adatkezelési-kérdések)

[**8. ADATVÉDELMI INCIDENS** 9](#adatvédelmi-incidens-1)

[**9. IRÁNYADÓ JOGSZABÁLYOK** 9](#irányadó-jogszabályok)

[**9. ADATKEZELÉSI TÁJÉKOZTATÓ MÓDOSÍTÁSA**
9](#adatkezelési-tájékoztató-módosítása)

# **1. ADATVÉDELMI TÁJÉKOZTATÓ CÉLJA, ALAPELVEI**

Adatvédelmi tájékoztatónk célja, hogy Ön mint Érintett a hatályos
jogszabályok előírásainak megfelelő és a lehető legszélesebb körű,
személyes adatainak kezelését/feldolgozását érintő tájékoztatást
megkapja a Kontúr Csoport Kft. (a továbbiakban: Társaság) valamennyi
tevékenységének gyakorlása, vagy szolgáltatásának igénybevétele során,
ide nem értve a Társasággal munkaviszonyban, vagy munkavégzésre irányuló
egyéb jogviszonyban álló személyek adatainak kezelésére/feldolgozására
vonatkozó esetet, amelyet Társaságunk belső szabályzatban rendez.

Célunknak megfelelően eljárásaink és feladatvégzésünk során biztosítjuk
az Érintettek személyes adatok védelméhez való jogának tiszteletben
tartását, az alábbi alapelvek szerint:

> a) a személyes adatokat jogszerűen és tisztességesen, valamint az
> Érintett számára átláthatóan kezeljük.
> 
> b) a személyes adatokat csak egyértelműen meghatározott, és jogszerű
> célból, jogaink gyakorlása és kötelezettségünk teljesítése érdekében
> gyűjtjük és azokat nem kezeljük a célokkal össze nem egyeztethető
> módon.
> 
> c) az általunk gyűjtött és kezelt személyes adatok az adatkezelés
> céljai szempontjából
> 
> elengedhetetlenek és a cél elérésére alkalmasak, valamint csak a
> szükséges mértékre korlátozódnak, és az adatok kezelése a cél
> elérése érdekében szükséges ideig történik.
> 
> d) Társaságunk minden észszerű intézkedést megtesz annak érdekében,
> hogy az általunk kezelt adatok pontosak, teljesek és szükség esetén
> naprakészek legyenek, a pontatlan személyes adatokat haladéktalanul
> töröljük vagy helyesbítjük.
> 
> e) a személyes adatokat olyan formában tároljuk, hogy az Érintett csak
> a személyes adatok kezelése céljainak eléréséhez szükséges ideig
> legyen azonosítható.
> 
> f) megfelelő technikai és szervezési intézkedések alkalmazásával
> biztosítjuk a személyes adatok megfelelő biztonságát az adatok
> jogosulatlan vagy jogellenes kezelésével, véletlen elvesztésével,
> megsemmisítésével vagy károsodásával szemben.

# 

# **2. ALAPFOGALMAK**

### **Adatkezelés**

Az alkalmazott eljárástól függetlenül az adatokon végzett bármely
művelet, például az adatok gyűjtése, felvétele, rögzítése,
rendszerezése, tárolása, megváltoztatása, felhasználása, munkaadó
számára történő továbbítása, lekérdezése, továbbítása, nyilvánosságra
hozatala, összehangolása vagy összekapcsolása, zárolása, törlése és
megsemmisítése, valamint az adatok további felhasználásának
megakadályozása, fénykép-, hang- vagy képfelvétel készítése, valamint
a személy azonosítására alkalmas fizikai jellemzők (ujj- vagy
tenyérnyomat, DNS-minta, íriszkép stb.) rögzítése.

### **Adatkezelő**

Az a természetes vagy jogi személy, illetve jogi személyiséggel nem
rendelkező szervezet, aki vagy amely - törvényben vagy az Európai Unió
kötelező jogi aktusában meghatározott keretek között - önállóan vagy
másokkal együtt az adat kezelésének célját meghatározza, az
adatkezelésre (beleértve a felhasznált eszközt) vonatkozó döntéseket
meghozza és végrehajtja, vagy az adatfeldolgozóval végrehajtatja.

### 

### **Adatfeldolgozás**

Az adatkezelő megbízásából vagy rendelkezése alapján eljáró
adatfeldolgozó által végzett adatkezelési műveletek összessége.

### **Adatfeldolgozó**

Az a természetes vagy jogi személy, illetve jogi személyiséggel nem
rendelkező szervezet, aki vagy amely – törvényben vagy az Európai Unió
kötelező jogi aktusában meghatározott keretek között és feltételekkel –
az adatkezelő megbízásából vagy rendelkezése alapján személyes adatokat
kezel.

### **Adatvédelmi incidens**

Az adatbiztonság olyan sérelme, amely a továbbított, tárolt vagy más
módon kezelt személyes adatok véletlen vagy jogellenes megsemmisülését,
elvesztését, módosulását, jogosulatlan továbbítását vagy nyilvánosságra
hozatalát, vagy az azokhoz való jogosulatlan hozzáférést eredményezi.

### **Címzett**

Az a természetes vagy jogi személy, illetve jogi személyiséggel nem
rendelkező szervezet, aki vagy amely részére személyes adatot az
adatkezelő, illetve az adatfeldolgozó hozzáférhetővé tesz.

### **Érintett**

Bármely információ alapján azonosított vagy azonosítható természetes
személy.

### **Felügyeleti hatóság (NAIH)**

Nemzeti Adatvédelmi és Információszabadság Hatóság (1125 Budapest,
Szilágyi Erzsébet fasor 22/c.)

### **Hozzájárulás**

Az érintett akaratának önkéntes, határozott és megfelelő tájékoztatáson
alapuló egyértelmű kinyilvánítása, amellyel az érintett nyilatkozat vagy
az akaratát félreérthetetlenül kifejező más magatartás útján jelzi, hogy
beleegyezését adja a rá vonatkozó személyes adatok kezeléséhez.

### **Személyes adat**

Az érintettre vonatkozó bármely információ.

# **3. ADATKEZELŐ**

| Adatkezelő neve:            | Kontúr Csoport Tervező, Építtető, Kivitelező Kereskedelmi Korlátolt Felelősségű Társaság      |
| --------------------------- | --------------------------------------------------------------------------------------------- |
| Adatkezelő rövidített neve: | Kontúr Csoport Kft.                                                                           |
| Adatkezelő székhelye:       | 2336 Dunavarsány, Gyóni Géza utca 4.                                                          |
| Adatkezelő fióktelepe:      | 1051 Budapest, Vörösmarty tér 1.                                                              |
| Adatkezelő e-mail címe:     | iroda@konturcsoport .hu                                                                       |
| Adatkezelő cégjegyzékszáma: | 13-09-161139                                                                                  |
| Adatkezelő adószáma:        | 12921092-2-13                                                                                 |
| Adatkezelő képviselői:      | Kovács Ambrus Dániel és Romhányi Péter László önálló cégjegyzési joggal rendelkező ügyvezetők |

Társaságunk a GDPR rendelet 37. cikke alapján nem köteles adatvédelmi
tisztviselő kinevezésére. Adatvédelmi és adatkezelési kérdésekben a
Társaság Jogi és Közbeszerzési Osztályvezetője, Pappné dr. Mazalin
Szilvia Éva áll az Ön rendelkezésére a <adatvédelem@konturcsoport.hu>
e-mail címen.

# **4. KEZELT ADATOK, ADATKEZELÉS JOGALAPJA, CÉLJA, HATÁRIDEJE**

Attól függően, hogy milyen célból kerülnek Társaságunkhoz a személyes
adatok, azokat eltérő mértékben, jogalapon és határidőig kezeljük, az
alábbiak szerint.

### **4.1. Önéletrajzok, bemutatkozó anyagok kezelése**

<table>
<thead>
<tr class="header">
<th><strong>Személyes adat</strong></th>
<th><strong>Adatkezelés célja</strong></th>
<th><strong>Adatkezelés jogalapja</strong></th>
<th><strong>Adatkezelés határideje</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>név, lakcím, telefonszám, e-mail-cím, képmás, valamint a dokumentumban közölt egyéb személyes adatok</td>
<td>Pályázat elbírálása, munkajogviszony létesítése</td>
<td><p>Érintett hozzájárulása</p>
<p>(GDPR 6. cikk (1) bek. a) pontja)</p></td>
<td>Érintett visszavonásáig</td>
</tr>
</tbody>
</table>

Adatait nem továbbítjuk más adatkezelőhöz, vagy adatfeldolgozóhoz. A
személyes adatok címzettjei: Társaságunk vezető tisztségviselői, vezető
asszisztensei, Jogi és Közbeszerzési Osztályvezetője, valamint az adott
pályázattal, önéletrajzzal érintett osztály vezetője.

### **4.2. Információkérés, ajánlatkérés során végzett adatkezelés**

<table>
<thead>
<tr class="header">
<th><strong>Személyes adat</strong></th>
<th><strong>Adatkezelés célja</strong></th>
<th><strong>Adatkezelés jogalapja</strong></th>
<th><strong>Adatkezelés határideje</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>név, telefonszám, e-mail-cím, valamint az Érintett által közölt egyéb személyes adatok</td>
<td>Ajánlatadás, kapcsolattartás</td>
<td><p>Érintett hozzájárulása</p>
<p>(GDPR 6. cikk (1) bek. a) pontja)</p></td>
<td>Érintett visszavonásáig, de legkésőbb az információ szolgáltatását, ajánlati kötöttség megszűntét követő 30 napig</td>
</tr>
</tbody>
</table>

Adatait nem továbbítjuk más adatkezelőhöz, vagy adatfeldolgozóhoz. A
személyes adatok címzettjei: Társaságunk vezető tisztségviselői, vezető
asszisztensei, Jogi és Közbeszerzési Osztályvezetője, valamint az adott
információkéréssel, ajánlatkéréssel érintett osztály vezetője.

### 

### **4.3. Társaság által üzemeltetett weboldallal összefüggő adatkezelés**

<table>
<thead>
<tr class="header">
<th><strong>Személyes adat</strong></th>
<th><strong>Adatkezelés célja</strong></th>
<th><strong>Adatkezelés jogalapja</strong></th>
<th><strong>Adatkezelés határideje</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>IP cím, a látogatás időpontja, az Ön által használt operációs rendszer és böngésző típusa</td>
<td>A honlap rendeltetésszerű és színvonalas működésének biztosítása, a rosszindulatú, weboldalunkat támadó látogatók beazonosítása</td>
<td><p>Társaságunk jogos érdeke - adataink és honlapunk biztonságának védelme</p>
<p>(GDPR 6. cikk (1) bek. f) pontja)</p></td>
<td>1 hónapig</td>
</tr>
</tbody>
</table>

Adatait nem továbbítjuk más adatkezelőhöz, vagy adatfeldolgozóhoz. A
személyes adatok címzettjei: Társaságunk vezető tisztségviselői, vezető
asszisztensei, Jogi és Közbeszerzési Osztályvezetője, valamint az
informatikai rendszerünk kezeléséért és biztonságáért felelős harmadik
személy (szerződéses partnerünk), a Brightdea Solutions Korlátolt
Felelősségű Társaság (székhely: 2142 Nagytarcsa, Boglárka utca 9., Cg.:
13-09-170724, képv.: Horváth Tamás ügyvezető, honlap: www.brightdea.hu).

### 

### **4.4. Szerződésekben feltüntetett személyes adatok kezelése**

<table>
<thead>
<tr class="header">
<th><strong>Személyes adat</strong></th>
<th><strong>Adatkezelés célja</strong></th>
<th><strong>Adatkezelés jogalapja</strong></th>
<th><strong>Adatkezelés határideje</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>név, telefonszám, e-mail-cím</td>
<td>Szerződésben foglalt kötelezettségek teljesítése, jogok gyakorlása</td>
<td><p>Társaságunk jogos érdeke – szerződéses rendelkezések bizonyíthatósága</p>
<p>(GDPR 6. cikk (1) bek. f) pontja)</p></td>
<td>A hatályos jogszabályok által meghatározott kötelezettségek teljesítési határidejéig</td>
</tr>
</tbody>
</table>

Adatait az adott szerződés teljesítésében részt vevő adatkezelőhöz
továbbítjuk, amelyről pontos, az adatkezelő személyére kiterjedő
tájékoztatást az adott szerződés megkötését megelőzően adunk. A
személyes adatok címzettjei: Társaságunk vezető tisztségviselői, vezető
asszisztensei, a szerződés – amennyiben az projekt megvalósítására
vonatkozik – tárgya szerinti projektvezető, a Vállalkozási és
Projektirányítási Osztályvezető, az Infrastruktúra Tervezési
Osztályvezető, a Biztosítóberendezési és Érősáram Tervezési
Osztályvezető, a Jogi és Közbeszerzési Osztályvezető és kollégái,
valamint a Gazdasági Osztályvezető és kollégái.

# **5. ADATKEZELÉS MÓDJA, ADATBIZTONSÁG**

Az Ön személyes adatait a hozzánk eljuttatott módon tároljuk, a papír
alapon érkező, személyes adatokat tartalmazó iratokat elektronikus
formában rögzítjük rendszerünkben oly módon, hogy azokhoz csak a
címzettek férhetnek hozzá.

Az adatkezelésre vonatkozó határidő leteltét követően a személyes
adatokat tartalmazó dokumentumokat helyreállíthatatlanul és véglegesen
megsemmisítjük (töröljük, illetve a papír alapú dokumentumokat
iratmegsemmisítővel tesszük a jövőre nézve használatra, olvasásra,
továbbításra alkalmatlanná).

A papír alapú dokumentumokat – a szerződések kivételével – a Jogi és
Közbeszerzési Osztály biztonsági ajtóval ellátott és zárt szobájában,
biztonsági, zárt szekrényben tároljuk. A papír alapú szerződéseket a
Pénzügyi Osztály biztonsági ajtóval ellátott, zárható szobájában
tartjuk.

Az elektronikusan rögzített dokumentumokat a Vault megnevezésű szoftver
használatával tároljuk, központi szerveren.

Adatait a hardvereink használatát biztosító egyedi jelszavakkal, illetve
az informatikai rendszerünket védő Bitdefender vírusírtóval és tűzfallal
védjük.

# 

# **6. AZ ÖN JOGAI, JOGORVOSLATI LEHETŐSÉGEI**

### **6.1. Az Ön jogai az adatkezelésünk során**

Önt mint Érintettet a személyes adatai vonatkozásában megilleti

1)  <span class="underline">az előzetes tájékozódáshoz való jog</span>:
    azaz az, hogy az adatkezeléssel összefüggő tényekről az adatkezelés
    megkezdését megelőzően tájékoztatást kapjon Társaságunktól,

2)  <span class="underline">a hozzáféréshez való jog</span>: azaz
    kérelmére személyes adatait és az azok kezelésével összefüggő
    információkat a rendelkezésére bocsátjuk,

3)  <span class="underline">a helyesbítéshez való jog</span>: amelynek
    megfelelően kérelmére, valamint a jogszabályban meghatározott
    további esetekben személyes adatait helyesbítjük, illetve
    kiegészítjük,

4)  <span class="underline">az adatkezelés korlátozásához (zároláshoz)
    való jog</span>: amely szerint kérelmére, valamint a jogszabályban
    meghatározott további esetekben személyes adatai kezelését
    korlátozzuk, így például (1) ha vitatja az adatai pontosságát (a
    pontos adat hitelt érdemlő igazolásáig), (2) ha az adatkezelésünk
    jogellenes, de nem kéri a törlést, hanem helyette a felhasználás
    korlátozását választja, (3) ha tiltakozik az adatkezelés ellen (amíg
    megállapításra kerül, hogy jogos indokaink elsőbbséget élveznek-e az
    Ön jogos indokaival szemben), vagy (4) már nincs szükségünk az
    adatai kezelésére, de Ön igényli azokat jogi igények
    előterjesztéséhez, érvényesítéséhez vagy védelméhez,

5)  <span class="underline">a tiltakozáshoz való jog</span>: amely
    alapján Ön személyes adatainak kezelését kifogásolja és az
    adatkezelés megszüntetését, illetve a kezelt adatok törlését
    kérheti, ez azonban nem illeti meg Önt a 4.3., illetve 4.4. pontban
    meghatározott adatkezelésünk esetén,

6)  <span class="underline">a törléshez való jog</span>: amelynek
    megfelelően kérelmére, valamint a jogszabályban meghatározott
    további esetekben személyes adatait töröljük,

7)  <span class="underline">panasztételi jog</span>: így Társaságunknál,
    vagy a felügyelő hatóságnál panaszt tehet, illetve eljárást
    kezdeményezhet,

8)  <span class="underline">bírósághoz fordulás joga</span>: amely
    alapján igényét bíróság előtt érvényesítheti.

### **6.2. Amennyiben Társaságunktól tájékoztatást kér**

  - az általunk kezelt személyes adatairól,

  - azok forrásáról,

  - az adatkezelés jogalapjáról és céljáról,

  - az adatkezelés időtartamáról, az időtartam meghatározására irányadó
    szempontokról,

  - személyes adatai továbbítása esetén az adattovábbítás címzettjéről
    és a továbbítás jogalapjáról,

  - az Önt érintő adatvédelmi incidens körülményéről, hatásáról, az
    elhárítására, valamint megelőzésére tett intézkedéseinkről, úgy

Önt késedelem nélkül, 10 napon – de legfeljebb 1 hónapon – belül
írásban, ingyenesen tájékoztatjuk. Önnek azonban a tájékoztatásért a
ráfordított munkaidőnkkel arányban álló ellenértéket kell fizetnie, ha
Ön a folyó évben azonos adatkörre vonatkozóan tájékoztatási kérelmet
már nyújtott be hozzánk. Az Ön által már megfizetett költségtérítést
visszatérítjük abban az esetben, ha az adatokat jogellenesen kezeltük
vagy a tájékoztatás kérése helyesbítéshez vezetett. A tájékoztatást csak
törvényben foglalt esetekben tagadhatjuk meg jogszabályi hely
megjelölésével, valamint a bírósági jogorvoslat illetve a Hatósághoz
fordulás lehetőségéről tájékoztatással egyidejűleg.

### **6.3. Amennyiben panaszával hozzánk fordul**

mindent elkövetünk annak érdekében, hogy a lehető leghamarabb, az Ön
számára is megnyugtató módon rendezzük kérését. Ennek körében
Társaságunk a személyes adatok helyesbítésről, zárolásról,
megjelölésről és törlésről Önt, továbbá mindazokat értesíti, akiknek
korábban az adatot adatkezelés céljára továbbította, kivéve akkor, ha az
értesítés elmaradása az Ön jogos érdekét nem sérti.

Amennyiben az Ön helyesbítés, zárolás vagy törlés iránti kérelmét nem
teljesítjük, a kérelem kézhezvételét követő 10 napon belül (legfeljebb
azonban 1 hónapon belül) írásban vagy – az Ön hozzájárulásával –
elektronikus úton közöljünk elutasításunk indokait és ezzel egyidejűleg
tájékoztatjuk Önt a bírósági jogorvoslat, továbbá a Hatósághoz fordulás
lehetőségéről.

Amennyiben Ön tiltakozik a személyes adatai kezelése ellen, a
tiltakozást a kérelem benyújtásától számított legrövidebb időn, 10
napon belül (legfeljebb azonban 1 hónapon belül) megvizsgáljuk és a
döntésünkről Önt írásban tájékoztatjuk. Amennyiben úgy döntöttünk,
hogy az Ön tiltakozása megalapozott, abban az esetben az adatkezelést –
beleértve a további adatfelvételt és adattovábbítást is – megszüntetjük
és az adatokat töröljük, vagy kérése esetén zároljuk, valamint a
tiltakozásról, továbbá az annak alapján tett intézkedésekről értesítjük
mindazokat, akik részére a tiltakozással érintett személyes adatot
korábban továbbítottuk, és akik kötelesek intézkedni a tiltakozási jog
érvényesítése érdekében.

Abban az esetben megtagadjuk a kérés teljesítését, ha bizonyítjuk, hogy
az adatkezelést olyan kényszerítő erejű jogos okok indokolják, amelyek
elsőbbséget élveznek az Ön érdekeivel, jogaival és szabadságaival
szemben, vagy amelyek jogi igények előterjesztéséhez, érvényesítéséhez
vagy védelméhez kapcsolódnak. Amennyiben Ön a döntésünkkel nem ért
egyet, vagy ha elmulasztjuk a határidőt, a döntés közlésétől, illetve a
határidő utolsó napjától számított 30 napon belül Ön bírósághoz
fordulhat.

### 

### **6.4. Amennyiben a felügyelő hatósághoz kíván fordulni**

úgy tájékoztatjuk az alábbiakról:

Felügyelő Hatóság: **Nemzeti Adatvédelmi és Információszabadság Hatóság
(NAIH)**

Székhely: 1125 Budapest, Szilágyi Erzsébet fasor 22/c.

Levelezési cím: 1530 Budapest, Pf.: 5.

Telefon: +36 (1) 391-1400

Fax: +36 (1) 391-1410

E-mail:<ugyfelszolgalat@naih.hu>

Honlap: <https://naih.hu/>

A hatóságnál benyújtandó panasz, illetve indítandó eljárás során
segítségére lehet a <https://naih.hu/panaszuegyintezes-rendje.html>.
dokumentum.

### **6.5. Amennyiben a bírósági eljárást választja**

ezt akkor teheti, ha megítélése szerint Társaságunk, illetve az általunk
megbízott vagy rendelkezésünk alapján eljáró adatfeldolgozó a személyes
adatait a személyes adatok kezelésére vonatkozó, jogszabályban vagy az
Európai Unió kötelező jogi aktusában meghatározott előírások
megsértésével kezeli.

Az adatvédelmi perek elbírálása a törvényszék hatáskörébe tartozik, a
per – az Ön választása szerint – az Ön lakhelye vagy tartózkodási helye
szerinti törvényszék előtt is megindítható, a Társaságunk székhelye
szerint illetékes törvényszék (Budapest Környéki Törvényszék) választása
helyett. AZ Ön lakóhelye, illetve tartózkodási helye szerint illetékes
törvényszék beazonosításához segítséget talál a
<https://birosag.hu/birosag-kereso> honlapon.

# **7. EGYÉB ADATKEZELÉSI KÉRDÉSEK**

Felhívjuk figyelmét arra, hogy amennyiben az adatkezelés az Ön
hozzájárulásán alapul, a személyes adat kezeléséhez való
hozzájárulását a Társaságunk jelen tájékoztató 3. pontjában
megjelölt elérhetőségén keresztül bármikor visszavonhatja.

A Társaságunkhoz került valamennyi olyan személyes adatot, amelynek nem
Társaságunk a címzettje, illetve amely kezeléséhez, feldolgozásához
jogalappal nem rendelkezünk (tévedésből, véletlenül hozzánk eljuttatott,
nekünk átadott adatok, hozzájárulás visszavonása), késedelem nélkül, az
Érintett lehetőség szerinti tájékoztatásával egyidejűleg,
helyreállíthatatlanul és véglegesen töröljük, illetve megsemmisítjük.

Amennyiben Ön – segítő szándékunk ellenére – úgy látja, hogy az
adatkezelésünkkel összefüggő jogait nem a Társaságunkhoz benyújtott
panasszal, hanem az igényérvényesítés egyéb módját választva kívánja
érvényesíteni, személyes adatait és az igényérvényesítéssel összefüggő
körülményeket a Társaságunkat képviselő Jogi és Közbeszerzési Osztályunk
kollégáival, esetenként külső megbízottal megosztjuk. Ez utóbbi esetben
Önt a külső megbízott személyéről és elérhetőségéről az eljárás
megindításáról való tudomásszerzésünket követő 15 napon belül
tájékoztatni fogjuk.

Tájékoztatjuk továbbá, hogy Társaságunk harmadik országba személyes
adatot nem továbbít.

# **8. ADATVÉDELMI INCIDENS**

Az Ön személyes adatait érintő adatvédelmi incidens történik, ha az
adatbiztonságunk oly módon sérül, amely a továbbított, tárolt vagy más
módon kezelt személyes adatai véletlen vagy jogellenes megsemmisülését,
elvesztését, módosulását, jogosulatlan továbbítását vagy nyilvánosságra
hozatalát, vagy az azokhoz való jogosulatlan hozzáférést eredményezi.
Amennyiben ezt Ön észleli, kérjük, a 3. pontban megjelölt elérhetőségen
ezt haladéktalanul jelezze felénk, hogy a megfelelő intézkedéseket
megtehessük a korrekció, illetve megelőzés érdekében.

Amennyiben az adatvédelmi incidenst mi észleljük az Ön személyes adati
vonatkozásában, arról jegyzőkönyvet veszünk fel, amelyben rögzítjük az
észlelő személy nevét, az incidens körülményeit, az incidens típusát,
időpontját, az érintettek személyét, valamint minden olyan körülményt,
amely a felügyelő hatóság felé történő bejelentés megtételéhez
szükséges. Az adatvédelmi incidenst haladéktalanul, de legfeljebb az
adatvédelmi incidensről való tudomásszerzését követő hetvenkét órán
belül bejelentjük a Hatóságnak.

Az adatvédelmi incidenst nem kell bejelentenünk, ha valószínűsíthető,
hogy az nem jár kockázattal az érintettek jogainak érvényesülésére.

Önt azonban haladéktalanul tájékoztatjuk az adatvédelmi incidensről,
amennyiben az valószínűsíthetően az Önt megillető valamely alapvető jog
érvényesülését lényegesen befolyásoló következményekkel járhat (magas
kockázatú adatvédelmi incidens). A tájékoztatással egyidőben
rendelkezésére bocsátjuk azon információkat is, hogy milyen
intézkedéseket tettünk az az incidens következményeinek
minimalizálása, valamint annak érdekében, hogy ez a jövőben ne
fordulhasson elő.

# **9. IRÁNYADÓ JOGSZABÁLYOK**

  - a természetes személyeknek a személyes adatok kezeléséről szóló az
    Európai Parlament és a Tanács (EU) 2016/679 rendelete (GDPR)

  - az információs önrendelkezési jogról és az információszabadságról
    szóló 2011. évi CXII. törvény - (Info tv.)

  - a Polgári Törvénykönyvről szóló 2013. évi V. törvény (Ptk.)

# 

# **9. ADATKEZELÉSI TÁJÉKOZTATÓ MÓDOSÍTÁSA**

Társaságunk fenntartja magának a jogot jelen Adatkezelési tájékoztató
módosítására, amelyről az érintetteket megfelelő módon tájékoztatja. Az
adatkezeléssel kapcsolatos információk közzététele a
www.konturcsoport.hu weboldalon történik.
